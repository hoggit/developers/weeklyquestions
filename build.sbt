import scala.sys.process._
lazy val akkaHttpVersion = "10.1.8"
lazy val akkaVersion    = "2.5.21"
lazy val hwDockerLogin= taskKey[Boolean]("Checks login status of the build.hoggitworld.com repo. Asks for creds if you're not logged in")
lazy val root = (project in file("."))
  .enablePlugins(JavaAppPackaging)
  .settings(
    inThisBuild(List(
      version         := "1.0",
      organization    := "com.hoggit",
      scalaVersion    := "2.12.8"
    )),
    scalacOptions := Seq(
      "-feature",
      "-deprecation",
      "-unchecked",
      "-Ywarn-unused-import",
      "-Ywarn-dead-code",
      "-Yno-adapted-args",
      "-Ywarn-dead-code"
    ),
    name := "weeklyquestions",
    mainClass in Compile := Some("com.hoggit.serverstatus.QuickstartServer"),
    dockerRepository := Some("registry.gitlab.com/hoggit"),
    dockerBaseImage := "openjdk:11-slim",
    publish in Docker := ((publish in Docker).dependsOn(hwDockerLogin)).value,
    hwDockerLogin := {
      val result = "bin/gitlab_reg_login.sh".!
      if (result != 0)
        throw new RuntimeException("Not logged into gitlab.com docker repo. Please login using `docker login registry.gitlab.com`")
      true
    },
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"          % akkaVersion,
      "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
      "io.spray"          %% "spray-json"           % "1.3.3",

      "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % Test,
      "com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion     % Test,
      "org.scalatest"     %% "scalatest"            % "3.0.5"         % Test
    )
  )
