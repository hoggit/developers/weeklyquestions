package com.hoggit.questions

import akka.actor._
import akka.http.scaladsl.model._
import akka.pattern.ask
import akka.testkit.{ ImplicitSender, TestKit }
import akka.util.Timeout
import com.hoggit.questions.http.HttpClient
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time._
import scala.concurrent.duration._

import scala.concurrent.Future
import spray.json._
class RedditThreadActorSpec extends TestKit(ActorSystem("redditthread")) with FlatSpecLike with Matchers with ImplicitSender
  with BeforeAndAfterAll
  with JsonSupport
  with ScalaFutures {
  implicit override val patienceConfig =
    PatienceConfig(timeout = Span(2, Seconds), interval = Span(5, Millis))
  implicit lazy val to = Timeout(5.seconds)
  import RedditThreadActor._

  class TestHttpClient(resp: HttpResponse) extends HttpClient {
    def request(req: HttpRequest) = Future.successful(resp)
  }

  override def afterAll(): Unit = shutdown(system)

  def httpResponseWithThreads(jsonResponse: RedditJsonResponse): HttpResponse = {
    HttpResponse(
      StatusCodes.OK,
      entity = jsonResponse.toJson.compactPrint)
  }

  def redditResponse(threads: Seq[RedditThread]): RedditJsonResponse = {
    RedditJsonResponse(
      RedditThreadsData(
        threads.map { t => RedditThreadKind(t) }))
  }

  abstract class RedditThreadActorTest() {
    def responseThreads: Seq[RedditThread]
    lazy val response = httpResponseWithThreads(redditResponse(responseThreads))
    def a = system.actorOf(RedditThreadActor.props(new TestHttpClient(response)))
    def getWeeklyThread = (a ? GetCurrentQuestionThread()).mapTo[Future[Option[RedditThread]]].flatten
  }

  "RedditThreadActor" must "find the first question thread" in new RedditThreadActorTest() {
    val weeklyThread = RedditThread("Weekly Questions Thread Mar 19", "http://reddit.com/r/hoggit/foobar", true)
    def responseThreads = Seq(weeklyThread)
    whenReady(getWeeklyThread) { thread =>
      thread shouldBe Some(weeklyThread)
    }
  }

  it should "return a None if there's no question thread" in new RedditThreadActorTest {
    val notWeeklyThread = RedditThread("Check out my cockpit", "http://reddit.com/r/hoggit/foobar", true)
    def responseThreads = Seq(notWeeklyThread)
    whenReady(getWeeklyThread) { thread =>
      thread shouldBe None
    }
  }

  it should "not return an unstickied question thread" in new RedditThreadActorTest {
    val weeklyThread = RedditThread("Weekly Questions Thread Mar 12", "http://reddit.com/r/hoggit/foobar", false)
    def responseThreads = Seq(weeklyThread)
    whenReady(getWeeklyThread) { thread =>
      thread shouldBe None
    }
  }

  it should "only return a stickied weekly thread from a list of threads" in new RedditThreadActorTest {
    val weeklyThread = RedditThread("Weekly Questions Thread Mar 19", "http://reddit.com/r/hoggit/foobar", true)
    val notWeeklyThread = RedditThread("Check out my cockpit", "http://reddit.com/r/hoggit/foobar", true)
    def responseThreads = Seq(weeklyThread) ++ Seq.fill(100)(notWeeklyThread)
    whenReady(getWeeklyThread) { thread =>
      thread shouldBe Some(weeklyThread)
    }
  }
}
