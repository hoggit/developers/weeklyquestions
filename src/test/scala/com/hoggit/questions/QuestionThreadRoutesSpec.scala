package com.hoggit.questions

import akka.actor.ActorRef
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.server._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.hoggit.questions.http._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ Matchers, FlatSpec }
import scala.concurrent._
import spray.json._

class QuestionThreadRoutesSpec extends FlatSpec with Matchers with ScalaFutures with ScalatestRouteTest {

  class TestHttpClient(resp: HttpResponse) extends HttpClient {
    def request(req: HttpRequest) = Future.successful(resp)
  }
  val as = system
  abstract class QuestionThreadRoutesTest extends QuestionThreadRoutes {
    def responseThreads: Seq[RedditThread]
    implicit def system = as
    def httpResponseWithThreads(jsonResponse: RedditJsonResponse): HttpResponse = {
      HttpResponse(
        StatusCodes.OK,
        entity = jsonResponse.toJson.compactPrint)
    }

    def redditResponse(threads: Seq[RedditThread]): HttpResponse = {
      httpResponseWithThreads(RedditJsonResponse(
        RedditThreadsData(
          threads.map { t => RedditThreadKind(t) })))
    }

    override def redditThreadActor: ActorRef =
      system.actorOf(RedditThreadActor.props(new TestHttpClient(redditResponse(responseThreads))))
  }

  "QuestionThreadRoutes" should "respond with a 302 and Location if a reddit questions thread was found" in new QuestionThreadRoutesTest {
    val responseThreads = Seq(RedditThread("Weekly Questions Thread Mar19", "http://example.com", true))
    Get("/") ~> userRoutes ~> check {
      responseAs[String] shouldEqual "http://example.com"
      status shouldEqual StatusCodes.Found
    }
  }

  it should "redirect to hoggit if we can't find a thread." in new QuestionThreadRoutesTest {
    val responseThreads = Seq()
    Get("/") ~> Route.seal(userRoutes) ~> check {
      status shouldEqual StatusCodes.Found
      header("Location").get match {
        case Location(loc) => loc shouldBe Uri("https://www.reddit.com/r/hoggit")
      }
    }
  }
}
