package com.hoggit.questions

import akka.actor._
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import com.hoggit.questions.http.HttpClient
import com.typesafe.scalalogging.StrictLogging
import scala.concurrent.Future
import spray.json._

final case class RedditJsonResponse(data: RedditThreadsData)
final case class RedditThreadsData(children: Seq[RedditThreadKind])
final case class RedditThreadKind(data: RedditThread)
final case class RedditThread(title: String, url: String, stickied: Boolean)

object RedditThreadActor {
  final case class GetCurrentQuestionThread()

  def props(client: HttpClient): Props = Props(new RedditThreadActor(client))
}

class RedditThreadActor(client: HttpClient) extends Actor with StrictLogging with JsonSupport {
  import RedditThreadActor._

  val weeklyThreadRegex = """^Weekly Questions Thread.+""".r
  implicit def ec = context.dispatcher
  implicit def am = ActorMaterializer()

  def getRedditThreads(): Future[HttpResponse] = {
    val req = HttpRequest(uri = "https://www.reddit.com/r/hoggit.json?limit=2")
    client.request(req)
  }

  def getQuestionThread(): Future[Option[RedditThread]] = {
    getRedditThreads() flatMap { resp =>
      Unmarshal(resp.entity).to[String]
    } map { s: String =>
      val threads: Seq[RedditThreadKind] = s.parseJson.convertTo[RedditJsonResponse].data.children
      threads find { t: RedditThreadKind =>
        weeklyThreadRegex.pattern.matcher(t.data.title).matches && t.data.stickied
      } map { _.data }
    }
  }

  def receive: Receive = {
    case GetCurrentQuestionThread() =>
      sender() ! getQuestionThread()
  }
}
