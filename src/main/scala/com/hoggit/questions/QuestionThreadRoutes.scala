package com.hoggit.questions

import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.Route
import scala.concurrent.duration._

import akka.pattern.ask
import akka.util.Timeout
import com.hoggit.questions.RedditThreadActor._
import scala.concurrent.ExecutionContext
import scala.concurrent.Future

//#user-routes-class
trait QuestionThreadRoutes extends JsonSupport {
  //#user-routes-class

  // we leave these abstract, since they will be provided by the App
  implicit def system: ActorSystem
  implicit def ec: ExecutionContext = system.dispatcher

  lazy val log = Logging(system, classOf[QuestionThreadRoutes])

  def redditThreadActor: ActorRef

  implicit lazy val timeout = Timeout(5.seconds) // usually we'd obtain the timeout from the system's configuration

  implicit def rejectionHandler: RejectionHandler = RejectionHandler.newBuilder()
    .handleNotFound {
      complete(HttpResponse(
        StatusCodes.Found,
        headers = headers.Location("https://www.reddit.com/r/hoggit") :: Nil))
    }.result

  def createResponse(thread: RedditThread): HttpResponse = {
    val uri = thread.url
    HttpResponse(
      StatusCodes.Found,
      headers = headers.Location(uri) :: Nil,
      entity = HttpEntity(contentType = ContentTypes.`text/plain(UTF-8)`, uri.getBytes()))
  }

  lazy val userRoutes: Route =
    pathSingleSlash {
      (rejectEmptyResponse & handleRejections(rejectionHandler)) {
        get {
          val threadFuture: Future[Option[RedditThread]] = (redditThreadActor ? GetCurrentQuestionThread()).mapTo[Future[Option[RedditThread]]].flatten
          val response: Future[Option[HttpResponse]] = threadFuture.map { opt: Option[RedditThread] =>
            opt map { createResponse(_) }
          }
          complete(response)
        }
      }
    }
}
