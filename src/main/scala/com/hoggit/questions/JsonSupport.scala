package com.hoggit.questions

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport {
  // import the default encoders for primitive types (Int, String, Lists etc)
  import DefaultJsonProtocol._

  implicit val redditThreadJsonFormat = jsonFormat3(RedditThread)
  implicit val redditThreadKind = jsonFormat1(RedditThreadKind)
  implicit val redditThreadsDataJsonFormat = jsonFormat1(RedditThreadsData)
  implicit val redditJsonResponseJsonFormat = jsonFormat1(RedditJsonResponse)

}
