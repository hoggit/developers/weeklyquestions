#!/bin/bash
set -e

echo "Checking login status of registry.gitlab.com"
if ! grep -q "registry.gitlab.com" ~/.docker/config.json ; then
    echo "Authentication not found. Please login using:"
    echo "docker login registry.gitlab.com"
    # Can't get interactive terminal in SBT.
    # Disable this for now, and just return a non-zero code that we can pickup in sbt
    #docker login "registry.gitlab.com"
    exit 1
else
  echo "Already logged in!"
  exit 0
fi

